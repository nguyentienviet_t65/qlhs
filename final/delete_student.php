```php
<?php
include 'app/config.php';
include APP_PATH . '/students.php';
include APP_PATH . '/database.php';

// Kết nối với cơ sở dữ liệu
$db = connect();

// Lấy ID học sinh cần xóa
$id = $_GET['id'];

// Xóa học sinh
deleteStudent($db, $id);

// Chuyển hướng về trang danh sách học sinh
header("Location: index.php");
exit;
```