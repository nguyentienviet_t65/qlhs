
```php
<?php
include 'app/config.php';
include APP_PATH . '/students.php';
include APP_PATH . '/database.php';

// Kết nối với cơ sở dữ liệu
$db = connect();

// Kiểm tra xem có lỗi nhập liệu không
$errors = [];
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['address'])) {
  // Lấy dữ liệu từ form
  $name = $_POST['name'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];
  $address = $_POST['address'];

  // Kiểm tra dữ liệu
  if (empty($name)) {
    $errors[] = "Họ tên không được để trống";
  }
  if (empty($email)) {
    $errors[] = "Email không được để trống";
  }
  if (empty($phone)) {
    $errors[] = "Điện thoại không được để trống";
  }
  if (empty($address)) {
    $errors[] = "Địa chỉ không được để trống";
  }

  // Nếu không có lỗi thì thêm học sinh vào cơ sở dữ liệu
  if (empty($errors)) {
    addStudent($db, $name, $email, $phone, $address);
    header("Location: index.php");
    exit;
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Thêm học sinh</title>
  <link rel="stylesheet" href="web/style.css">
</head>
<body>
  <h1>Thêm học sinh</h1>

  <?php if (!empty($errors)): ?>
    <div class="error">
      <?php foreach ($errors as $error): ?>
        <p><?php echo $error; ?></p>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>

  <form action="" method="POST">
    <label for="name">Họ tên:</label>
    <input type="text" name="name" id="name">

    <label for="email">Email:</label>
    <input type="email" name="email" id="email">

    <label for="phone">Điện thoại:</label>
    <input type="text" name="phone" id="phone">

    <label for="address">Địa chỉ:</label>
    <input type="text" name="address" id="address">

    <input type="submit" value="Thêm">
  </form>
</body>
</html>
```