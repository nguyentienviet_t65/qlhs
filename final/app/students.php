```php
<?php
function getAllStudents($db) {
  $sql = "SELECT * FROM students";
  $stmt = $db->prepare($sql);
  $stmt->execute();
  return $stmt->fetchAll();
}

function getStudentById($db, $id) {
  $sql = "SELECT * FROM students WHERE id = ?";
  $stmt = $db->prepare($sql);
  $stmt->execute([$id]);
  return $stmt->fetch();
}

function addStudent($db, $name, $email, $phone, $address) {
  $sql = "INSERT INTO students (name, email, phone, address, created_at, updated_at) VALUES (?, ?, ?, ?, NOW(), NOW())";
  $stmt = $db->prepare($sql);
  $stmt->execute([$name, $email, $phone, $address]);
}

function updateStudent($db, $id, $name, $email, $phone, $address) {
  $sql = "UPDATE students SET name = ?, email = ?, phone = ?, address = ?, updated_at = NOW() WHERE id = ?";
  $stmt = $db->prepare($sql);
  $stmt->execute([$name, $email, $phone, $address, $id]);
}

function deleteStudent($db, $id) {
  $sql = "DELETE FROM students WHERE id = ?";
  $stmt = $db->prepare($sql);
  $stmt->execute([$id]);
}
```