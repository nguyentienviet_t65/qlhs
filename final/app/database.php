
```php
<?php
function connect() {
  $host = "localhost";
  $user = "root";
  $pass = "";
  $dbname = "student_management";

  $dsn = "mysql:host=$host;dbname=$dbname";

  try {
    $db = new PDO($dsn, $user, $pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $db;
  } catch (PDOException $e) {
    echo "Kết nối cơ sở dữ liệu thất bại: " . $e->getMessage();
  }
}
```