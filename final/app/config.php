
```php
<?php

// Đường dẫn đến thư mục gốc của dự án
define('BASE_PATH', dirname(__DIR__));

// Đường dẫn đến thư mục `app`
define('APP_PATH', BASE_PATH . '/app');

// Đường dẫn đến thư mục `web`
define('WEB_PATH', BASE_PATH . '/web');

// Đường dẫn đến file `sql/create_table.sql`
define('SQL_FILE', BASE_PATH . '/sql/create_table.sql');
```