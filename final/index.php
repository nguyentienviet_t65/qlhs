
```php
<?php
include 'app/config.php';
include APP_PATH . '/students.php';
include APP_PATH . '/database.php';

// Kết nối với cơ sở dữ liệu
$db = connect();

// Lấy danh sách học sinh
$students = getAllStudents($db);
?>

<!DOCTYPE html>
<html>
<head>
  <title>Quản lý học sinh</title>
  <link rel="stylesheet" href="web/style.css">
</head>
<body>
  <h1>Danh sách học sinh</h1>

  <a href="add_student.php">Thêm học sinh</a>

  <table>
    <thead>
      <tr>
        <th>ID</th>
        <th>Họ tên</th>
        <th>Email</th>
        <th>Điện thoại</th>
        <th>Địa chỉ</th>
        <th>Ngày tạo</th>
        <th>Ngày cập nhật</th>
        <th>Hành động</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($students as $student): ?>
        <tr>
          <td><?php echo $student['id']; ?></td>
          <td><?php echo $student['name']; ?></td>
          <td><?php echo $student['email']; ?></td>
          <td><?php echo $student['phone']; ?></td>
          <td><?php echo $student['address']; ?></td>
          <td><?php echo $student['created_at']; ?></td>
          <td><?php echo $student['updated_at']; ?></td>
          <td>
            <a href="edit_student.php?id=<?php echo $student['id']; ?>">Sửa</a>
            <a href="delete_student.php?id=<?php echo $student['id']; ?>">Xóa</a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <form action="search_student.php" method="GET">
    <input type="text" name="keyword" placeholder="Nhập từ khóa">
    <input type="submit" value="Tìm kiếm">
  </form>
</body>
</html>
```